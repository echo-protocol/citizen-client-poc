const JSBN =  require('jsbn');
const BigInt = JSBN.BigInteger;

const agoraClient = require('./agoraClient');
const BBSClient = require('./BBSClient');
const users  = require('./services/users');
const votes = require('./services/votes');
const proposals = require('./services/proposals');
const elgamal  = require('./services/elgamal');
const proofs = require('./services/proofs');

const validPlaintexts = [2, 3, 5, 7, 11, 13, 17, 19, 23];
const password = 'pwd';

exports.usersCount = null;
exports.proposal = null;
exports.choices = null;
exports.upvotesNeeded = null;
exports.voteStart = null;
exports.voteDuration = null;

exports. run = async function (upvotesCount, votesCount){

  let t0 = Date.now();

  let choicesList = validPlaintexts.slice(0, exports.choices.length);

  const usersList = await registerUsers(exports.usersCount, password, []);

  const propId = await registerProposal(usersList[0], password, exports.proposal, exports.choices, exports.upvotesNeeded, exports.voteStart, exports.voteDuration)

  await usersUpvoteProposal(upvotesCount, password, propId, usersList, 0);


  let prop = JSON.parse(await getProposal(usersList[0], password, propId));

  let vote = JSON.parse(await getVotes())[prop[propId].voteId];

  if(vote) {
    await castBallots(usersList, password, votesCount, vote, choicesList, 0);

    let t1 = Date.now();

    let delay = 0;

    if(t1 - t0 < exports.voteDuration) delay = exports.voteDuration - t1 + t0;

    setTimeout(async () => {
      vote = JSON.parse(await getVotes())[prop[propId].voteId];

      console.log('VOTE RESULT: \n')
      console.log(exports.proposal + '\n');

      for (let i = 0; i < exports.choices.length; i++) {
        console.log(exports.choices[i] + ' --> ' + vote.tally[i])
      }

      console.log('\n')

    }, delay)
  } else{
    console.log('\nPROPOSAL ' + propId + ' DID NOT GET APPROVED YET, VOTE IS NOT DEPLOYED\n')
  }



};

async function castBallots(usersList, password, votesCount, vote, choicesList, index){
  return new Promise(async (resolve, reject) => {
    try{

      if(usersList.length < votesCount) votesCount = usersList.length;

      if(index === 0){
        vote.pubKey.p = new BigInt(vote.pubKey.p, 10);
        vote.pubKey.g = new BigInt(vote.pubKey.g, 10);
        vote.pubKey.A = new BigInt(vote.pubKey.A, 10);

      }
      if(index < votesCount) {

        let m = choicesList[parseInt(choicesList.length * Math.random(), 10)];

        let result = await castBallot(usersList[index], vote, m);

        if(result.status) console.log('BALLOT CAST SUCCESSFULLY\n');
        else {
          switch(result.reason) {
            case 0:
              console.log('BALLOT REJECTED: VOTE CLOSED\n');
              break;
            case 1:
              console.log('BALLOT REJECTED: VOTER AUTHENTICATION FAILED\n');
              break;
            case 2:
              console.log('BALLOT REJECTED: POKEK VERIFICATION FAILED\n');
              break
            case 3:
              console.log('BALLOT REJECTED: DZKPOP VERIFICATION FAILED\n');
            default:
              console.log('BALLOT REJECTED: REASON UNKNOWN\n');
          }
        }

        resolve(await castBallots(usersList, password, votesCount, vote, choicesList, index + 1))

      } else {
        resolve()
      }
    } catch (e) { reject(e) }
  })
}

async function usersUpvoteProposal(upvotesCount, password, propId, usersList, index){
  return new Promise(async (resolve, reject) => {
    try{
      if(index < upvotesCount){
        await upvoteProposal(usersList[index], password, propId)
        resolve(await usersUpvoteProposal(upvotesCount, password, propId, usersList, index + 1))

      } else {
        resolve()

      }
    } catch (e) { reject(e) }
  })
}

async function registerUsers(usersCount, password, usersList){
  return new Promise(async (resolve, reject) => {
    try{

       if(usersList.length < usersCount){
          usersList.push(await registerNewUser(password));
          resolve(await registerUsers(usersCount, password, usersList))

       } else {
          resolve(usersList)

       }

    } catch (e) { reject(e) }
  })
}


async function registerUsersAndUpvoteProposal(usersCount, password, propId, usersList){
  return new Promise(async (resolve, reject) => {
    try{
      if(usersList.length === 0){
        const { propId, userId } = await registerUserAndRegisterProposal();
        await upvoteProposal(userId, password, propId);
        usersList.push(userId);
        resolve(await registerUsersAndUpvoteProposal(usersCount, password, propId, usersList))

      } else if(usersList.length < usersCount){
        usersList.push(await registerUserAndUpvoteProposal(propId));
        resolve(await registerUsersAndUpvoteProposal(usersCount, password, propId, usersList))

      } else {
        resolve({ usersList, propId })

      }
    } catch (e) { reject(e) }
  })
}

async function registerUserAndRegisterProposal(){

  const userId = await registerNewUser(password);

  const propId = await registerProposal(userId, password, exports.proposal, exports.choices, exports.upvotesNeeded, exports.voteStart, exports.voteDuration);

  return { propId, userId }
}

async function registerUserAndUpvoteProposal(propId){

  const userId = await registerNewUser(password);

  await upvoteProposal(userId, password, propId);

  return userId

}

async function registerNewUser(password){
      const res = await agoraClient.registerUser(password);
      users.addUser(res.userId, password);
      console.log('User registered: ' + res.userId +'\n');
      return res.userId
}

async function registerProposal(userId, password, proposal, choices, upvotesNeeded, voteStart, voteDuration){
  const res = await agoraClient.registerProposal(userId, password, proposal, choices, upvotesNeeded, voteStart, voteDuration);
  const prop = {
    proposal,
    choices,
    upvotesNeeded,
    voteStart,
    voteDuration
  };
  proposals.addProposal(res.propId, prop);
  console.log('Proposal registered: ' + res.propId +'\n');
  return res.propId
}

async function getProposal(userId, password, propId){
  const proposal =  await agoraClient.getProposal(userId, password, propId);
  return proposal
}

async function upvoteProposal(userId, password, propId){
  return await agoraClient.upvoteProposal(userId, password, propId)
}

async function getVotes(){
  return await BBSClient.getVotes()
}

async function castBallot(userId, vote, m){

  console.log('USER ' + userId + ' CASTING BALLOT ' + m + 'FOR VOTE ' + vote.voteId + '\n');

  const { c1BigInt, c2BigInt, kBigInt } =  await elgamal.encryptMessage(m, vote.pubKey.p, vote.pubKey.g, vote.pubKey.A);

  const nipokek = await proofs.generatePOKOfEphemeralKey(kBigInt, vote.pubKey.g, c1BigInt, vote.pubKey.p);

  const nidzkpop = await proofs.generateDisjunctiveZKProofOfPlaintext(kBigInt, m, vote.choicesList, vote.pubKey.g, vote.pubKey.A, c2BigInt, vote.pubKey.p);

  return await BBSClient.castBallot(userId, vote.voteId, c1BigInt, c2BigInt, nipokek, nidzkpop);

}
