const request = require('./utils/request');

require('dotenv').config();

exports.getVotes = function(){

  const uri = 'http://' + process.env.BALLOT_BOX_SERVER_URI + '/votes';

  return request.sendGETRequest(uri);

};


exports.castBallot = async function(userId, voteId, c1, c2, nipokek, nidzkpop){

  const uri = 'http://' + process.env.BALLOT_BOX_SERVER_URI + '/cast';

  let commitments = [];
  let challenges = [];
  let responses = [];

  for(let i = 0; i < nidzkpop.commitments.length; i++){
    commitments.push(nidzkpop.commitments[i].toString(10));
    challenges.push(nidzkpop.challenges[i].toString(10));
    responses.push(nidzkpop.responses[i].toString(10));
  }

  const body = {
    voteId,
    userId,
    c1: c1.toString(10),
    c2: c2.toString(10),
    nipokek: {
      aBigInt: nipokek.aBigInt.toString(10),
      eBigInt: nipokek.eBigInt.toString(10),
      zBigInt: nipokek.zBigInt.toString(10),
    },
    nidzkpop: {
      commitments,
      challenges,
      responses
    }
  };

  const result =  await request.sendPOSTRequest(uri, body);

  return result

};

