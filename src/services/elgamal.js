const JSBN =  require('jsbn');
const BigInt = JSBN.BigInteger;
const Utils = require('../utils/utils');

exports.encryptMessage = async function (m, pBigInt, gBigInt, ABigInt){
  return new Promise(async (resolve, reject) => {
    try{

      const mBigInt  = new BigInt(m.toString(), 10);

      const kBigInt = await Utils.getRandomNbitBigIntAsync(2048); // EPHEMERAL KEY

      // console.log("Ephemeral Key k = ", kBigInt.toString(10) + "\n");

      let c1BigInt = gBigInt.modPow(kBigInt, pBigInt);// CIPHERTEXT 1

      let c2BigInt;

      c2BigInt = mBigInt.multiply(ABigInt.modPow(kBigInt, pBigInt)).remainder(pBigInt); // CIPHERTEXT 2

      // console.log("Ciphertext (c1, c2) =", c1BigInt.toString(10), c2BigInt.toString(10), "\n");

      resolve({ c1BigInt, c2BigInt, kBigInt })

    } catch(e) { reject(e) }
  })
};
