const request = require('./utils/request');

require('dotenv').config();

exports.registerUser = function(password){

      const url = 'http://' + process.env.AGORA_BACKEND_URI + '/register';

      const body = {
        registrationKey: process.env.REGISTRATION_KEY,
        password: password
      };

      const res = request.sendPOSTRequest(url, body);

      if(res.status && res.status === 'error') return res.status;

      return res

};

/**
 * This function sends an HTTP request to the Agora Backend Service to register a new vote proposal
 * @param userId
 * @param password
 * @param proposal
 * @param choices
 * @param upvotesNeeded
 * @param voteStart
 * @param voteDuration
 * @returns {Promise<*>}
 */
exports.registerProposal = function(userId, password, proposal, choices, upvotesNeeded, voteStart, voteDuration){

  const url = 'http://' + process.env.AGORA_BACKEND_URI + '/proposal';

  const body = {
    userId,
    password,
    proposal,
    choices,
    upvotesNeeded,
    voteStart,
    voteDuration
  };

  const res = request.sendPOSTRequest(url, body);

  return res

};


exports.getProposal = function(userId, password, propId){

  const url = 'http://' + process.env.AGORA_BACKEND_URI + '/proposal?userId=' + userId + '&password=' + password + '&propId=' + propId;

  const proposal = request.sendGETRequest(url);

  return proposal

};

exports.upvoteProposal = function(userId, password, propId){

  const url = 'http://' + process.env.AGORA_BACKEND_URI + '/upvote';

  const body = {
    userId,
    password,
    propId
  };

  const proposal = request.sendPOSTRequest(url, body);

  return proposal

};
