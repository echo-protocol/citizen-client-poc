const app = require('./app');


/** VOTANTS **/
app.usersCount = 45;

/** PROPOSITION DE VOTE **/
app.proposal = 'Pour ou contre le RIC?'; // INTITULE
app.choices = ['Pour :]', 'Contre :[', "Quand-est-ce qu'on mange?"]; // CHOIX POSSIBLES
app.upvotesNeeded = 30; // NOMBRE D'UPVOTES NECESSAIRES AU DEPLOIEMENT DU VOTE

/** DETAILS DU VOTE **/
app.voteStart = Date.now(); // DEBUT DU VOTE
app.voteDuration = 1000*10; // DUREE DU VOTE

/** VOTES **/
const upVotesCount = 32; // NOMBRE D'UPVOTES POUR LA PROPOSITION DE VOTE

const votesCount = 12; // NOMBRE DE VOTES

app.run(upVotesCount, votesCount);
