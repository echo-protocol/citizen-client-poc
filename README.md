# Citizen Client POC

A simple NodeJS POC client for the VoDI Engine POC server


## Install
```bash
git clone https://gitlab.com/echo-protocol/citizen-client-poc.git

npm install
```

## Run
```bash
npm start
```
## Generate docs
```bash
npm run-script doc
```

## Install and Run with Docker
```bash
docker build ./docker -t citizen-client:1.0.0
```
